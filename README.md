# community-service

#### 介绍
织羽智慧社区 安全 高效 智能，多社区管理，一套系统支持N个社区入住
*社区管理、业主入住、房屋绑定、访客通行 投诉表扬 社区交流 在线报修 在线缴费 咨询建议 通知公告等*
后期开放:
- 智能门禁 使用蓝牙或者人脸识别门禁，无需携带钥匙,便捷管理。原有社区及单元门禁改动方便，成本低。未来社区门禁发展趋势，更安全、更高效
- 智能摄像头 24小时实时监控，实力保障社区环境安全
- 智慧停车场 实现车库在线缴费、在线查询、自动缴费等功能，提高物业管理效率，节约停车时间，实现无感出入
- 社区O2O 线上广告 社区团购 以及周边其他商业，一站式服务，为您打造
- 该社区完全开源 详情加入qq群：651439161
- 注：部分功能正在建设，开发测试完成会全部开源

#### 软件架构
前后端分离
- springboot框架基石
- Minio 图片存储服务
- SpringMVC控制层
- MybatisPlus持久层
- Redis缓存
- Mysql5.7数据库
- 短信服务使用阿里云短信服务
前端
- vue2.x
- vue-element-admin
小程序(或App)
- uniapp
- uViewUI

#### 后台体验地址
[点击访问：http://zhiyucloud.cn:8081/](http://zhiyucloud.cn:8081/)

#### Web端gitee地址
[https://gitee.com/hebei-zhiyu-network/community-web.git](https://gitee.com/hebei-zhiyu-network/community-web.git)

### 小程序地址
[https://gitee.com/hebei-zhiyu-network/Zy-WcMini.git](https://gitee.com/hebei-zhiyu-network/Zy-WcMini.git)
#### 小程序体验请进群增加体验员
qq群：651439161

#### 运行截图
![image](docs/img/1.jpg)
![image](docs/img/2.jpg)
![image](docs/img/3.jpg)
![image](docs/img/4.jpg)

![image](docs/img/6.png)

![image](docs/img/8.png)


#### 安装教程

加群联系管理员

#### 使用说明

加群联系管理员

#### 感谢

- [感谢若依：https://doc.ruoyi.vip/](https://doc.ruoyi.vip/)
- [感谢uni-app：https://uniapp.dcloud.io/](https://uniapp.dcloud.io/)
- [感谢vue-element-admin：https://panjiachen.github.io/vue-element-admin-site/zh/：](https://panjiachen.github.io/vue-element-admin-site/zh/)
- [感谢uviewUi：https://www.uviewui.com/components/intro.html](https://www.uviewui.com/components/intro.html)

- [开源不易，如果对你有所帮助，请作者吃个馒头吧]
- ![image](docs/img/5.png)


