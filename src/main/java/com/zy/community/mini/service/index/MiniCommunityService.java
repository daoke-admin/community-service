package com.zy.community.mini.service.index;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.community.domain.ZyCommunity;
import com.zy.community.community.mapper.ZyCommunityMapper;
import com.zy.community.web.controller.mini.index.dto.CommunityDto;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class MiniCommunityService {

    @Resource
    private ZyCommunityMapper zyCommunityMapper;

   /**
    * 获取所有小区信息
    * @return
    */
    public ZyResult<List<CommunityDto>> findAllCommunity() {
        List<ZyCommunity> zyCommunities = zyCommunityMapper.selectList(new QueryWrapper<ZyCommunity>()
                .select("community_id","community_name").orderByAsc("create_time"));
        List<CommunityDto> collect = zyCommunities.stream().map(zyCommunity -> {
            CommunityDto communityDto = new CommunityDto();
            communityDto.setCommunityId(zyCommunity.getCommunityId());
            communityDto.setCommunityName(zyCommunity.getCommunityName());
            return communityDto;
        }).collect(Collectors.toList());
        return ZyResult.data(collect);
    }

}
