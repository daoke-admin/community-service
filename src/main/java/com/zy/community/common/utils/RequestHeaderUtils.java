package com.zy.community.common.utils;

import com.zy.community.common.constant.HttpStatus;
import com.zy.community.common.exception.CustomException;
import org.springframework.beans.factory.annotation.Value;

import javax.servlet.http.HttpServletRequest;

/**
 * @author yangdi
 */
public class RequestHeaderUtils {

    public static Long GetCommunityId(String propertyId){
        Long communityId = null;
        HttpServletRequest request = ServletUtils.getRequest();
        try {
            String communityIdStr = request.getHeader(propertyId);
            communityId = Long.valueOf(communityIdStr);
        } catch (Exception e) {
            new CustomException("参数communityId有误", HttpStatus.UNSUPPORTED_TYPE);
        }
        return communityId;
    }
}
