package com.zy.community.common.core.domain;

import com.zy.community.common.utils.DateUtils;
import com.zy.community.common.utils.SecurityUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Date;

/**
 * @author yangdi
 */
public class ManageParametersUtils {

    /**
     * 保存参数处理
     * @param t
     * @param <T>
     * @return
     */
    public static <T> T saveMethod(T t){
        Class<?> aClass = t.getClass();
        try {
            Method setCreateBy = aClass.getMethod("setCreateBy", String.class);
            setCreateBy.invoke(t, SecurityUtils.getUsername());

            Method setCreateTime = aClass.getMethod("setCreateTime", Date.class);
            setCreateTime.invoke(t, DateUtils.getNowDate());

            Method setUpdateBy = aClass.getMethod("setUpdateBy", String.class);
            setUpdateBy.invoke(t, SecurityUtils.getUsername());

            Method setUpdateTime = aClass.getMethod("setUpdateTime", Date.class);
            setUpdateTime.invoke(t, DateUtils.getNowDate());

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return t;
    }

    /**
     * 修改参数配置
     * @param t
     * @param <T>
     * @return
     */
    public static <T> T updateMethod(T t){
        Class<?> aClass = t.getClass();
        try {
            Method setUpdateBy = aClass.getMethod("setUpdateBy", String.class);
            setUpdateBy.invoke(t, SecurityUtils.getUsername());

            Method setUpdateTime = aClass.getMethod("setUpdateTime", Date.class);
            setUpdateTime.invoke(t, DateUtils.getNowDate());

        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        return t;
    }

}
