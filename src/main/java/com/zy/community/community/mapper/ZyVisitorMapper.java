package com.zy.community.community.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zy.community.community.domain.ZyVisitor;
import com.zy.community.web.controller.mini.index.dto.VisitorDto;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import com.zy.community.community.domain.dto.ZyVisitorDto;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 访客邀请 Mapper接口
 *
 * @author yangdi
 * @date 2020-12-18
 */
public interface ZyVisitorMapper extends BaseMapper<ZyVisitor> {

    @Select("select visitor_id as visitorId,community_id as communityId,visitor_name as visitorName,visitor_phone_number as visitorPhone," +
            "visitor_date as visitorDate from zy_visitor where community_id=#{communityId} and create_by_open_id=#{openId} " +
            "order by create_time desc")
    List<VisitorDto> findVisitorsByPage(@Param("communityId") Long communityId,@Param("openId") String openId);
    /**
     * 查询列表
     * @param zyVisitor
     * @return
     */
    @Select("<script>" +
            "select" +
            " a.visitor_id, a.visitor_name, a.visitor_phone_number," +
            " a.visitor_date, a.create_by_id, a.create_by_open_id," +
            " a.create_by, a.create_time, a.update_by, a.update_time," +
            " a.remark, a.community_id, b.community_name as communityName from zy_visitor a\n" +
            "LEFT JOIN zy_community b on a.community_id = b.community_id " +
            "<where>" +
            "<if test=\"visitorPhoneNumber !=null and visitorPhoneNumber != ''\">" +
            "and a.visitor_phone_number like concat('%',#{visitorPhoneNumber},'%') " +
            "</if>" +
            "<if test=\"visitorName !=null and visitorName != ''\">" +
            "and a.visitor_name like concat('%',#{visitorName},'%') " +
            "</if>" +
            "and a.community_id = #{communityId}" +
            "</where>" +
            " order by a.create_time DESC" +
            "</script>")
    List<ZyVisitorDto> selectZyVisitorList(ZyVisitor zyVisitor);
}
