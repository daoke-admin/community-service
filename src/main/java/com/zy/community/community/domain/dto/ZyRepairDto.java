package com.zy.community.community.domain.dto;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zy.community.common.annotation.Excel;
import com.zy.community.common.core.domain.BaseEntity;
import com.zy.community.community.domain.vo.RepairState;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 报修信息对象 zy_repair
 * 
 * @author yin
 * @date 2020-12-22
 */
public class ZyRepairDto extends BaseEntity

{

    /** 报修ID */
    @TableId
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long repairId;

    /** 报修编号 */
    @Excel(name = "报修编号")
    private String repairNum;

    /** 报修状态 */
    @Excel(name = "报修状态")
    private RepairState repairState;

    /** 派单时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "派单时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date assignmentTime;

    /** 接单时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "接单时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date receivingOrdersTime;

    /** 处理完成时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "处理完成时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date completeTime;

    /** 取消时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "取消时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date cancelTime;

    /** 期待上门时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "期待上门时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date doorTime;

    /** 分派人id */
    @Excel(name = "分派人id")
    private Long assignmentId;

    /** 处理人id */
    @Excel(name = "处理人id")
    private Long completeId;

    /** 处理人电话 */
    @Excel(name = "处理人电话")
    private String completePhone;

    /** 处理人姓名 */
    @Excel(name = "处理人姓名")
    private String completeName;

    /** 创建人id */
    @Excel(name = "创建人id")
    private Long userId;

    /** 删除状态0默认1删除 */
    private Integer delFlag;

    /** 报修内容 */
    @Excel(name = "报修内容")
    private String repairContent;

    /** 小区ID */
    @Excel(name = "小区ID")
    private Long communityId;

    /** 详细地址 */
    @Excel(name = "详细地址")
    private String address;

    /**用户真实姓名*/
    private String ownerRealName;

    /**业主电话*/
    private String ownerPhoneNumber;

    public String getOwnerRealName() {
        return ownerRealName;
    }

    public void setOwnerRealName(String ownerRealName) {
        this.ownerRealName = ownerRealName;
    }

    public String getOwnerPhoneNumber() {
        return ownerPhoneNumber;
    }

    public void setOwnerPhoneNumber(String ownerPhoneNumber) {
        this.ownerPhoneNumber = ownerPhoneNumber;
    }

    public void setRepairId(Long repairId)
    {
        this.repairId = repairId;
    }

    public Long getRepairId() 
    {
        return repairId;
    }
    public void setRepairNum(String repairNum) 
    {
        this.repairNum = repairNum;
    }

    public String getRepairNum() 
    {
        return repairNum;
    }
    public void setRepairState(RepairState repairState)
    {
        this.repairState = repairState;
    }

    public RepairState getRepairState()
    {
        return repairState;
    }
    public void setAssignmentTime(Date assignmentTime) 
    {
        this.assignmentTime = assignmentTime;
    }

    public Date getAssignmentTime() 
    {
        return assignmentTime;
    }
    public void setReceivingOrdersTime(Date receivingOrdersTime) 
    {
        this.receivingOrdersTime = receivingOrdersTime;
    }

    public Date getReceivingOrdersTime() 
    {
        return receivingOrdersTime;
    }
    public void setCompleteTime(Date completeTime) 
    {
        this.completeTime = completeTime;
    }

    public Date getCompleteTime() 
    {
        return completeTime;
    }
    public void setCancelTime(Date cancelTime) 
    {
        this.cancelTime = cancelTime;
    }

    public Date getCancelTime() 
    {
        return cancelTime;
    }
    public void setDoorTime(Date doorTime) 
    {
        this.doorTime = doorTime;
    }

    public Date getDoorTime() 
    {
        return doorTime;
    }
    public void setAssignmentId(Long assignmentId) 
    {
        this.assignmentId = assignmentId;
    }

    public Long getAssignmentId() 
    {
        return assignmentId;
    }
    public void setCompleteId(Long completeId) 
    {
        this.completeId = completeId;
    }

    public Long getCompleteId() 
    {
        return completeId;
    }
    public void setCompletePhone(String completePhone) 
    {
        this.completePhone = completePhone;
    }

    public String getCompletePhone() 
    {
        return completePhone;
    }
    public void setCompleteName(String completeName) 
    {
        this.completeName = completeName;
    }

    public String getCompleteName() 
    {
        return completeName;
    }
    public void setUserId(Long userId) 
    {
        this.userId = userId;
    }

    public Long getUserId() 
    {
        return userId;
    }
    public void setDelFlag(Integer delFlag) 
    {
        this.delFlag = delFlag;
    }

    public Integer getDelFlag() 
    {
        return delFlag;
    }
    public void setRepairContent(String repairContent) 
    {
        this.repairContent = repairContent;
    }

    public String getRepairContent() 
    {
        return repairContent;
    }
    public void setCommunityId(Long communityId) 
    {
        this.communityId = communityId;
    }

    public Long getCommunityId() 
    {
        return communityId;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }

    @Override
    public String toString() {
        return "ZyRepairDto{" +
                "repairId=" + repairId +
                ", repairNum='" + repairNum + '\'' +
                ", repairState=" + repairState +
                ", assignmentTime=" + assignmentTime +
                ", receivingOrdersTime=" + receivingOrdersTime +
                ", completeTime=" + completeTime +
                ", cancelTime=" + cancelTime +
                ", doorTime=" + doorTime +
                ", assignmentId=" + assignmentId +
                ", completeId=" + completeId +
                ", completePhone='" + completePhone + '\'' +
                ", completeName='" + completeName + '\'' +
                ", userId=" + userId +
                ", delFlag=" + delFlag +
                ", repairContent='" + repairContent + '\'' +
                ", communityId=" + communityId +
                ", address='" + address + '\'' +
                ", ownerRealName='" + ownerRealName + '\'' +
                ", ownerPhoneNumber='" + ownerPhoneNumber + '\'' +
                '}';
    }
}
