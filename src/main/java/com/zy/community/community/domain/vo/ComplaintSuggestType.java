package com.zy.community.community.domain.vo;

public enum ComplaintSuggestType {
    /**
     * 投诉
     */
    Complaint,
    /**
     * 建议
     */
    Suggest;
}
