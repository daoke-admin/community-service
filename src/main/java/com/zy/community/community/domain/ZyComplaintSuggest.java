package com.zy.community.community.domain;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zy.community.common.annotation.Excel;
import com.zy.community.common.core.domain.BaseEntity;
import com.zy.community.community.domain.vo.ComplaintSuggestType;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

/**
 * 投诉建议 对象 zy_complaint_suggest
 *
 * @author yangdi
 * @date 2020-12-18
 */
public class ZyComplaintSuggest extends BaseEntity {
    private static final long serialVersionUID = 1L;

    /**
     * id
     */
    @TableId
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long complaintSuggestId;

    /**
     * 小区id
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long communityId;

    /**
     * 类型(投诉、建议)
     */
    @Excel(name = "类型(投诉、建议)")
    private ComplaintSuggestType complaintSuggestType;

    /**
     * 内容
     */
    @Excel(name = "内容")
    private String complaintSuggestContent;
    /**
     * 创建人Id,即投诉建议人
     */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long userId;

    public void setComplaintSuggestId(Long complaintSuggestId) {
        this.complaintSuggestId = complaintSuggestId;
    }

    public static long getSerialVersionUID() {
        return serialVersionUID;
    }

    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public Long getComplaintSuggestId() {
        return complaintSuggestId;
    }

    public ComplaintSuggestType getComplaintSuggestType() {
        return complaintSuggestType;
    }

    public void setComplaintSuggestType(ComplaintSuggestType complaintSuggestType) {
        this.complaintSuggestType = complaintSuggestType;
    }

    public void setComplaintSuggestContent(String complaintSuggestContent) {
        this.complaintSuggestContent = complaintSuggestContent;
    }

    public String getComplaintSuggestContent() {
        return complaintSuggestContent;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.MULTI_LINE_STYLE)
                .append("complaintSuggestId", getComplaintSuggestId())
                .append("communityId", getCommunityId())
                .append("complaintSuggestType", getComplaintSuggestType())
                .append("complaintSuggestContent", getComplaintSuggestContent())
                .append("createBy", getCreateBy())
                .append("createTime", getCreateTime())
                .append("updateBy", getUpdateBy())
                .append("updateTime", getUpdateTime())
                .append("remark", getRemark())
                .toString();
    }
}
