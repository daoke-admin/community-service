package com.zy.community.community.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.util.List;

/**
 * @author yangdi
 */
public class KnDto {
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long code;

    private String name;

    private List<KnDto> children;

    public Long getCode() {
        return code;
    }

    public void setCode(Long code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<KnDto> getChildren() {
        return children;
    }

    public void setChildren(List<KnDto> children) {
        this.children = children;
    }

    @Override
    public String toString() {
        return "KnDto{" +
                "code=" + code +
                ", name='" + name + '\'' +
                ", children=" + children +
                '}';
    }
}
