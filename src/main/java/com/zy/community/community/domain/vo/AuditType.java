package com.zy.community.community.domain.vo;

public enum AuditType {
    /**
     * web端用户审核
     */
    Web,
    /**
     * 移动端用户审核
     */
    App;
}
