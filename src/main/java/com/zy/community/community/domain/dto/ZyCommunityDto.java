package com.zy.community.community.domain.dto;

import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.zy.community.common.annotation.Excel;
import com.zy.community.common.core.domain.BaseEntity;
import com.zy.community.community.domain.ZyCommunity;

/**
 * @author yangdi
 */
public class ZyCommunityDto extends BaseEntity {

    /** 小区id */
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long communityId;

    /** 小区名称 */
    private String communityName;

    /** 小区编码 */
    private String communityCode;

    /** 省区划码 */
    private String communityProvenceCode;

    private String communityProvenceName;

    /** 市区划码 */
    private String communityCityCode;

    private String communityCityName;

    /** 区县区划码 */
    private String communityTownCode;

    private String communityTownName;

    /** 详细地址 */
    private String communityDetailedAddress;

    /** 经度 */
    private String communityLongitude;

    /** 纬度 */
    private String communityLatitude;

    /** 物业id */
    private Long deptId;

    /** 排序 */
    private Long communitySort;

    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }

    public String getCommunityCode() {
        return communityCode;
    }

    public void setCommunityCode(String communityCode) {
        this.communityCode = communityCode;
    }

    public String getCommunityProvenceCode() {
        return communityProvenceCode;
    }

    public void setCommunityProvenceCode(String communityProvenceCode) {
        this.communityProvenceCode = communityProvenceCode;
    }

    public String getCommunityProvenceName() {
        return communityProvenceName;
    }

    public void setCommunityProvenceName(String communityProvenceName) {
        this.communityProvenceName = communityProvenceName;
    }

    public String getCommunityCityCode() {
        return communityCityCode;
    }

    public void setCommunityCityCode(String communityCityCode) {
        this.communityCityCode = communityCityCode;
    }

    public String getCommunityCityName() {
        return communityCityName;
    }

    public void setCommunityCityName(String communityCityName) {
        this.communityCityName = communityCityName;
    }

    public String getCommunityTownCode() {
        return communityTownCode;
    }

    public void setCommunityTownCode(String communityTownCode) {
        this.communityTownCode = communityTownCode;
    }

    public String getCommunityTownName() {
        return communityTownName;
    }

    public void setCommunityTownName(String communityTownName) {
        this.communityTownName = communityTownName;
    }

    public String getCommunityDetailedAddress() {
        return communityDetailedAddress;
    }

    public void setCommunityDetailedAddress(String communityDetailedAddress) {
        this.communityDetailedAddress = communityDetailedAddress;
    }

    public String getCommunityLongitude() {
        return communityLongitude;
    }

    public void setCommunityLongitude(String communityLongitude) {
        this.communityLongitude = communityLongitude;
    }

    public String getCommunityLatitude() {
        return communityLatitude;
    }

    public void setCommunityLatitude(String communityLatitude) {
        this.communityLatitude = communityLatitude;
    }

    public Long getDeptId() {
        return deptId;
    }

    public void setDeptId(Long deptId) {
        this.deptId = deptId;
    }

    public Long getCommunitySort() {
        return communitySort;
    }

    public void setCommunitySort(Long communitySort) {
        this.communitySort = communitySort;
    }

    @Override
    public String toString() {
        return "ZyCommunityDto{" +
                "communityId=" + communityId +
                ", communityName='" + communityName + '\'' +
                ", communityCode='" + communityCode + '\'' +
                ", communityProvenceCode='" + communityProvenceCode + '\'' +
                ", communityProvenceName='" + communityProvenceName + '\'' +
                ", communityCityCode='" + communityCityCode + '\'' +
                ", communityCityName='" + communityCityName + '\'' +
                ", communityTownCode='" + communityTownCode + '\'' +
                ", communityTownName='" + communityTownName + '\'' +
                ", communityDetailedAddress='" + communityDetailedAddress + '\'' +
                ", communityLongitude='" + communityLongitude + '\'' +
                ", communityLatitude='" + communityLatitude + '\'' +
                ", deptId=" + deptId +
                ", communitySort=" + communitySort +
                '}';
    }
}
