package com.zy.community.community.service.impl;

import java.util.Arrays;
import java.util.List;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.zy.community.common.constant.HttpStatus;
import com.zy.community.common.core.domain.ManageParametersUtils;
import com.zy.community.common.exception.CustomException;
import com.zy.community.common.utils.DateUtils;
import com.zy.community.common.utils.RequestHeaderUtils;
import com.zy.community.common.utils.SecurityUtils;
import com.zy.community.community.domain.ZyOwner;
import com.zy.community.community.domain.ZyOwnerRoom;
import com.zy.community.community.domain.ZyOwnerRoomRecord;
import com.zy.community.community.domain.dto.ZyOwnerDto;
import com.zy.community.community.domain.vo.AuditType;
import com.zy.community.community.domain.vo.RoomStatus;
import com.zy.community.community.mapper.ZyOwnerMapper;
import com.zy.community.community.mapper.ZyOwnerRoomMapper;
import com.zy.community.community.mapper.ZyOwnerRoomRecordMapper;
import com.zy.community.community.service.IZyOwnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * 业主 Service业务层处理
 *
 * @author yangdi
 * @date 2020-12-16
 */
@Service
public class ZyOwnerServiceImpl implements IZyOwnerService {
    @Resource
    private ZyOwnerMapper zyOwnerMapper;
    @Resource
    private ZyOwnerRoomMapper zyOwnerRoomMapper;
    @Resource
    private ZyOwnerRoomRecordMapper zyOwnerRoomRecordMapper;

    @Value("${community.value}")
    private String propertyId;

    /**
     * 查询业主 列表
     *
     * @param zyOwner 业主
     * @return 业主
     */
    @Override
    public List<ZyOwnerDto> selectZyOwnerList(ZyOwner zyOwner) {
        Long communityId = RequestHeaderUtils.GetCommunityId(propertyId);

        return zyOwnerRoomMapper.queryZyOwnerList(communityId);
    }

    @Override
    public int deleteZyOwnerByIds(Long ownerRoomId) {

        ZyOwnerRoom zyOwnerRoom = zyOwnerRoomMapper.selectById(ownerRoomId);
        if (zyOwnerRoom == null){
            throw new CustomException("参数ownerRoomId有误", HttpStatus.UNSUPPORTED_TYPE);
        }
        ZyOwnerRoomRecord zyOwnerRoomRecord = new ZyOwnerRoomRecord();
        zyOwnerRoomRecord.setRoomStatus(RoomStatus.Unbind);
        zyOwnerRoomRecord.setCommunityId(zyOwnerRoom.getCommunityId());
        zyOwnerRoomRecord.setBuildingId(zyOwnerRoom.getBuildingId());
        zyOwnerRoomRecord.setUnitId(zyOwnerRoom.getUnitId());
        zyOwnerRoomRecord.setRoomId(zyOwnerRoom.getRoomId());
        zyOwnerRoomRecord.setOwnerId(zyOwnerRoom.getOwnerId());
        zyOwnerRoomRecord.setOwnerType(zyOwnerRoom.getOwnerType());
        zyOwnerRoomRecord.setRecordAuditType(AuditType.Web);
        zyOwnerRoomRecord.setCreateById(SecurityUtils.getLoginUser().getUser().getUserId());
        zyOwnerRoomRecordMapper.insert(ManageParametersUtils.saveMethod(zyOwnerRoomRecord));
        return zyOwnerRoomMapper.deleteById(ownerRoomId);
    }
}
