package com.zy.community.web.controller.mini.index;

import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.mini.service.index.MiniComplaintsSuggestionService;
import com.zy.community.web.controller.mini.index.dto.ComplaintSuggestionDto;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.Valid;

@RestController
@RequestMapping("/mini/community/cs")
public class MiniComplaintSuggestionController {
    @Resource
    private MiniComplaintsSuggestionService miniComplaintsSuggestionService;

    @PostMapping("/save")
    public ZyResult<String> saveCS(@RequestBody @Valid ComplaintSuggestionDto dto, BindingResult result){
        if (result.hasErrors()){
            return ZyResult.fail(400,result.getFieldError().getDefaultMessage());
        }
        return miniComplaintsSuggestionService.saveComplainSuggestion(dto);
    }
}
