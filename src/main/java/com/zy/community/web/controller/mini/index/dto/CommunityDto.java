package com.zy.community.web.controller.mini.index.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 小区Dto
 */
public class CommunityDto {
    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long communityId;

    private String communityName;


    public Long getCommunityId() {
        return communityId;
    }

    public void setCommunityId(Long communityId) {
        this.communityId = communityId;
    }

    public String getCommunityName() {
        return communityName;
    }

    public void setCommunityName(String communityName) {
        this.communityName = communityName;
    }
}
