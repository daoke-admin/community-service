package com.zy.community.web.controller.mini.community;

import com.github.pagehelper.PageInfo;
import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.community.domain.ZyComment;
import com.zy.community.mini.service.community.interaction.MiniCommunityInteractionService;
import com.zy.community.web.controller.mini.community.dto.*;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/mini/community/interaction")
public class MiniInteractionController {
    @Resource
    private MiniCommunityInteractionService miniCommunityInteractionService;

    @PostMapping("/save")
    public ZyResult<String> saveInteraction(@RequestBody CommunityInteractionDto dto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ZyResult.fail(400, bindingResult.getFieldError().getDefaultMessage());
        }
        return miniCommunityInteractionService.savePostTopic(dto);
    }

    @GetMapping("/{communityId}")
    public ZyResult<List<InteractionDto>> findTop4(@PathVariable("communityId") Long communityId) {
        return miniCommunityInteractionService.findTop4Interaction(communityId);
    }

    @GetMapping("/page/{page}/{size}/{communityId}")
    public ZyResult<PageInfo<InteractionDetailDto>> findPageInfo(@PathVariable("page") Integer page,
                                                           @PathVariable("size") Integer size,
                                                           @PathVariable("communityId") Long communityId) {
        return miniCommunityInteractionService.findInteractionByPage(page, size, communityId);
    }


    @GetMapping("/detail/{id}")
    public ZyResult<InteractionDetailDto> findById(@PathVariable("id") Long id) {
        return miniCommunityInteractionService.findById(id);
    }

    @PostMapping("/comment")
    public ZyResult<ZyComment> postComment(@RequestBody @Valid CommentRequestDto dto, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            return ZyResult.fail(400, bindingResult.getFieldError().getDefaultMessage());
        }
        return miniCommunityInteractionService.postComment(dto);
    }

    @GetMapping("/allComments/{interactionId}")
    public ZyResult<List<CommentRootDto>> findAllComment(@PathVariable("interactionId") Long interactionId) {
        return miniCommunityInteractionService.findCommentList(interactionId);
    }
}
