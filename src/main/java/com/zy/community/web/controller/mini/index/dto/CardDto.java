package com.zy.community.web.controller.mini.index.dto;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;

/**
 * 身份卡数据
 */
public class CardDto implements Serializable {

    @JsonFormat(shape = JsonFormat.Shape.STRING)
    private Long ownerRoomId;

    private String ownerType;//业主类型

    private String ownerRealName;//业主真实姓名

    private String buildingName;//楼栋名称

    private String unitName;//单元名称

    private String roomName;//房屋名称


    public Long getOwnerRoomId() {
        return ownerRoomId;
    }

    public void setOwnerRoomId(Long ownerRoomId) {
        this.ownerRoomId = ownerRoomId;
    }

    public String getOwnerType() {
        return ownerType;
    }

    public void setOwnerType(String ownerType) {
        this.ownerType = ownerType;
    }

    public String getOwnerRealName() {
        return ownerRealName;
    }

    public void setOwnerRealName(String ownerRealName) {
        this.ownerRealName = ownerRealName;
    }

    public String getBuildingName() {
        return buildingName;
    }

    public void setBuildingName(String buildingName) {
        this.buildingName = buildingName;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }
}
