package com.zy.community.web.controller.community;

import java.util.List;

import com.zy.community.common.annotation.Log;
import com.zy.community.common.core.controller.BaseController;
import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.common.core.page.TableDataInfo;
import com.zy.community.common.enums.BusinessType;
import com.zy.community.common.utils.poi.ExcelUtil;
import com.zy.community.community.domain.ZyRoom;
import com.zy.community.community.domain.dto.ZyRoomDto;
import com.zy.community.community.service.IZyRoomService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * 房间 Controller
 * 
 * @author ruoyi
 * @date 2020-12-11
 */
@Api(tags = "房间")
@RestController
@RequestMapping("/system/room")
public class ZyRoomController extends BaseController
{
    @Resource
    private IZyRoomService zyRoomService;

    /**
     * 查询房间 列表
     */
    @ApiOperation(value = "查询房间")
    @PreAuthorize("@ss.hasPermi('system:room:list')")
    @GetMapping("/list")
    public TableDataInfo list(ZyRoom zyRoom)
    {
        startPage();
        List<ZyRoomDto> list = zyRoomService.selectZyRoomList(zyRoom);
        return getDataTable(list);
    }

    /**
     * 导出房间 列表
     */
    @ApiOperation(value = "导出房间")
    @PreAuthorize("@ss.hasPermi('system:room:export')")
    @Log(title = "房间 ", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public ZyResult export(ZyRoom zyRoom)
    {
        List<ZyRoomDto> list = zyRoomService.selectZyRoomList(zyRoom);
        ExcelUtil<ZyRoomDto> util = new ExcelUtil<ZyRoomDto>(ZyRoomDto.class);
        return util.exportExcel(list, "room");
    }

    /**
     * 获取房间 详细信息
     */
    @ApiOperation(value = "获取房间")
    @PreAuthorize("@ss.hasPermi('system:room:query')")
    @GetMapping(value = "/{roomId}")
    public ZyResult getInfo(@PathVariable("roomId") Long roomId)
    {
        return ZyResult.data(zyRoomService.selectZyRoomById(roomId));
    }

    /**
     * 新增房间 
     */
    @ApiOperation(value = "新增房间")
    @PreAuthorize("@ss.hasPermi('system:room:add')")
    @Log(title = "房间 ", businessType = BusinessType.INSERT)
    @PostMapping
    public ZyResult add(@RequestBody ZyRoom zyRoom)
    {
        return toAjax(zyRoomService.insertZyRoom(zyRoom));
    }

    /**
     * 修改房间 
     */
    @ApiOperation(value = "修改房间")
    @PreAuthorize("@ss.hasPermi('system:room:edit')")
    @Log(title = "房间 ", businessType = BusinessType.UPDATE)
    @PutMapping
    public ZyResult edit(@RequestBody ZyRoom zyRoom)
    {
        return toAjax(zyRoomService.updateZyRoom(zyRoom));
    }

    /**
     * 删除房间 
     */
    @ApiOperation(value = "删除房间")
    @PreAuthorize("@ss.hasPermi('system:room:remove')")
    @Log(title = "房间 ", businessType = BusinessType.DELETE)
	@DeleteMapping("/{roomIds}")
    public ZyResult remove(@PathVariable Long[] roomIds)
    {
        return toAjax(zyRoomService.deleteZyRoomByIds(roomIds));
    }
}
