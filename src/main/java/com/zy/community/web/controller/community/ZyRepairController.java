package com.zy.community.web.controller.community;
import com.zy.community.common.annotation.Log;
import com.zy.community.common.core.controller.BaseController;
import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.common.core.page.TableDataInfo;
import com.zy.community.common.enums.BusinessType;
import com.zy.community.common.utils.poi.ExcelUtil;
import com.zy.community.community.domain.ZyRepair;
import com.zy.community.community.domain.dto.ZyRepairDto;
import com.zy.community.community.service.IZyRepairService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.util.List;

/**
 * 报修信息Controller
 * 
 * @author yin
 * @date 2020-12-22
 */
@RestController
@RequestMapping("/system/repair")
public class ZyRepairController extends BaseController
{
    @Autowired
    private IZyRepairService zyRepairService;

    /**
     * 查询报修信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:repair:list')")
    @GetMapping("/list")
    public TableDataInfo list(ZyRepairDto zyRepair)
    {
        startPage();
        List<ZyRepairDto> list = zyRepairService.selectZyRepairList(zyRepair);
        return getDataTable(list);
    }

    /**
     * 导出报修信息列表
     */
    @PreAuthorize("@ss.hasPermi('system:repair:export')")
    @Log(title = "报修信息", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public ZyResult export(ZyRepairDto zyRepair)
    {
        List<ZyRepairDto> list = zyRepairService.selectZyRepairList(zyRepair);
        ExcelUtil<ZyRepairDto> util = new ExcelUtil<ZyRepairDto>(ZyRepairDto.class);
        return util.exportExcel(list, "repair");
    }

    /**
     * 获取报修信息详细信息
     */
    @PreAuthorize("@ss.hasPermi('system:repair:query')")
    @GetMapping(value = "/{repairId}")
    public ZyResult getInfo(@PathVariable("repairId") Long repairId)
    {
        return ZyResult.data(zyRepairService.selectZyRepairById(repairId));
    }

    /**
     * 新增报修信息
     */
    @PreAuthorize("@ss.hasPermi('system:repair:add')")
    @Log(title = "报修信息", businessType = BusinessType.INSERT)
    @PostMapping
    public ZyResult add(@RequestBody ZyRepair zyRepair)
    {
        return toZyAjax(zyRepairService.insertZyRepair(zyRepair));
    }

    /**
     * 修改报修信息
     */
    @PreAuthorize("@ss.hasPermi('system:repair:edit')")
    @Log(title = "报修信息", businessType = BusinessType.UPDATE)
    @PutMapping
    public ZyResult edit(@RequestBody ZyRepair zyRepair)
    {
        return toZyAjax(zyRepairService.updateZyRepair(zyRepair));
    }

    /**
     * 删除报修信息
     */
    @PreAuthorize("@ss.hasPermi('system:repair:remove')")
    @Log(title = "报修信息", businessType = BusinessType.DELETE)
	@DeleteMapping("/{repairIds}")
    public ZyResult remove(@PathVariable Long[] repairIds)
    {
        return toZyAjax(zyRepairService.deleteZyRepairByIds(repairIds));
    }
}
