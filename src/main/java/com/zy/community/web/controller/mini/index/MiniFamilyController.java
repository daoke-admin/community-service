package com.zy.community.web.controller.mini.index;

import com.zy.community.common.core.domain.r.ZyResult;
import com.zy.community.mini.service.index.MiniFamilyService;
import com.zy.community.web.controller.mini.index.dto.FamilyDto;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/mini/community/family")
public class MiniFamilyController {
    @Resource
    private MiniFamilyService miniFamilyService;

    /**
     * 查询住户信息
     * @param communityId 社区Id
     * @return 家庭成员信息
     */
    @GetMapping("/info/{communityId}")
    public ZyResult<List<FamilyDto>> findFamilyInfo(@PathVariable("communityId") Long communityId){
        return miniFamilyService.findFamilyInfo(communityId);
    }

    /**
     * 解绑
     * @param roomId 房间Id
     * @param ownerId 用户Id
     * @return 解绑结果
     */
    @DeleteMapping("/unBind/{roomId}/{ownerId}")
    public ZyResult<String> unBindOption(@PathVariable("roomId") Long roomId,@PathVariable("ownerId") Long ownerId){
        return miniFamilyService.unBindOption(roomId,ownerId);
    }
}
